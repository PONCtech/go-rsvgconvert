# go-rsvgconvert

A simple wrapper around rsvg-convert
## Example use

```
import (
	"gitlab.com/PONCtech/go-rsvgconvert"
)

...

pdf, err := rsvgconvert.ToPdf(svgData)
```

Full example [here](example/main.go).

