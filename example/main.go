package main

import (
	"bytes"
	"io"
	"log"
	"os"

	"gitlab.com/PONCtech/go-rsvgconvert"
)

func main() {
	input := os.Stdin
	var err error

	if len(os.Args) == 2 {
		if os.Args[1] == "-" {
			input = os.Stdin
		} else {
			input, err = os.Open(os.Args[1])
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	var b bytes.Buffer
	_, err = io.Copy(&b, input)

	if err != nil {
		log.Fatal(err)
	}

	data := b.Bytes()

	pdf, err := rsvgconvert.ToPDF(data)
	if err != nil {
		log.Fatal(err)
	}

	outbuffer := bytes.NewBuffer(pdf)

	_, err = io.Copy(os.Stdout, outbuffer)

	if err != nil {
		log.Fatal(err)
	}
}
