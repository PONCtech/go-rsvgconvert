package rsvgconvert

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
)

func ToPDF(svg []byte) (output []byte, err error) {
	tmpfile, err := ioutil.TempFile("", "")
	if err != nil {
		return nil, fmt.Errorf("Could not create temp file: %w", err)
	}
	defer os.Remove(tmpfile.Name())

	_, err = tmpfile.Write(svg)
	if err != nil {
		return nil, fmt.Errorf("Problem writing svg data: %w", err)
	}

	cmd := exec.Command("rsvg-convert", "-f", "pdf", tmpfile.Name())
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err = cmd.Run()
	if err != nil {
		return nil, fmt.Errorf("%w: %s", err, string(stderr.Bytes()))
	}

	return stdout.Bytes(), err
}
